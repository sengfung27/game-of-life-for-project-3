/*mun
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& vec2);
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

vector<vector<bool> > vec2;
vector<bool> vec1;
vector<vector<bool> > nvec2;
vector<bool> nvec1;

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

size_t nbrCount(size_t i,size_t j, const vector<vector<bool> >& vec2)
{
	int width = vec2[i].size();
	int length= vec2[j].size();
	int total = 0;

		if(vec2[i][(j-1+width)%width]== true)
			total++;
		if(vec2[i][(j+1+width)%width]== true)
			total++;
		if(vec2[(i-1+length)%length][j]== true)
			total++;
		if(vec2[(i+1+length)%length][j]== true)
			total++;
		if(vec2[(i-1+length)%length][(j-1+width)%width]== true)
			total++;
		if(vec2[(i+1+length)%length][(j-1+width)%width]== true)
			total++;
		if(vec2[(i-1+length)%length][(j+1+width)%width]== true)
			total++;
		if(vec2[(i+1+length)%length][(j+1+width)%width]== true)
			total++;

			return total;
		total = 0;
	}

		/*switch(total){
		case 3: if(total==3)
		g[i][j]==true;
		break;
		case 2: if(total==2)
		if(g[i][j]==true)
		{
		g[i][j]==true;
		}
		else
		{
		g[i][j]==false;
		}
		break;
		default:
		g[i][j]==false;
		break;
									}*/





int initFromFile(const string& fname)//old
{
	const char *fname2 = fname.c_str();
	FILE* f = fopen(fname2,"rb");
	vector<vector<bool> > vec2;
	vector<bool> vec1;
	char c ;
	while(fread(&c,1,1,f)!=0){
	if(c=='\n'){
		vec2.push_back(vec1);
		vec1.clear();
	}
	else{
		if(c=='.')
			vec1.push_back(false);
		else
			vec1.push_back(true);

		}
		}
	fclose(f);
		return 0;
	}

void dumpState(FILE* f)
{
	 //FILE* f= fopen(,"wb");
	char c;

	while(fwrite(&c,1,1,f)!=0){
		if(c=='\n'){
		nvec2.push_back(nvec1);
		nvec1.clear();
	}
		else{
		if(c=='.')
			nvec1.push_back(false);
		else
			nvec1.push_back(true);
}
}


	}



void update(size_t i,size_t j,int total){
	switch(total){
		case 3: if(total==3)
		vec2[i][j]=='o';
		break;
		case 2: if(total==2)
		if(vec2[i][j]=='o')
		{
		vec2[i][j]=='o';
		
		}
		else
		{
		vec2[i][j]=='.';
	
		}
		break;
		default:
		vec2[i][j]=='.';
		break;
									}



}
void mainLoop() {
	/* update, write, sleep */
	initFromFile("/tmp/gol-world-current");
	int total;
	for(int j = 0; j< vec2.size(); j++){

			for(int i = 0; i < vec1.size(); i++){
			total = nbrCount(i,j,vec2);
			update(i,j,total);











			}
		}

		 dumpState();
}